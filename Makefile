CC=c++

all: impl.o article.o
	$(CC) -std=c++11 -Wall -o program src/main.cpp -I./src src/impl.o src/article.o
impl.o: src/lib/impl.cpp src/lib/impl.hpp
	$(CC) -g -c -std=c++11 -Wall src/lib/impl.cpp -o ./src/impl.o
article.o: src/lib/article.cpp src/lib/article.hpp
	$(CC) -g -c -std=c++11 -Wall src/lib/article.cpp -o ./src/article.o
clean:
	rm src/*.o
run: all
	clear
	./program