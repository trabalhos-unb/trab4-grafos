#include <vector>
#include <cstdlib>
#include <iostream>
#include <ctime>

#include "graph.hpp"

#define DEFAULT_SUBSAMPLE_SIZE 256
#define DEFAULT_NUMBER_TREES 100

#ifndef ARTICLE
#define ARTICLE
class Vertice{

};


typedef std::vector<std::vector<Aresta>> MtxAdj;
typedef std::vector<Aresta> ArestaAdj;
typedef std::vector<MtxAdj> Forest;

namespace Article{
    Forest iforest(MtxAdj , const int , const int );
    int path_length(float, MtxAdj &, const int,int);
}

#endif