#include "article.hpp"

MtxAdj sample(MtxAdj , const int );
void itree(MtxAdj &, const int);
float getSplitPoint(MtxAdj &);
void filter(MtxAdj &, MtxAdj &, const float );

Forest Article::iforest(MtxAdj data, const int number_trees, const int subsample_s){
   Forest forest; 
   MtxAdj subgrafo;

   Forest::iterator iterator;
   iterator = forest.begin();

   for(int i = 0; i < number_trees ; i++){
       subgrafo = sample(data, subsample_s);
       itree(subgrafo, subsample_s);
       forest.insert(iterator,subgrafo);
   }

   return forest;
};

void itree(MtxAdj &subtree, const int subsample_s){
    int splitPoint = 0;
    MtxAdj subLeft, subRight;

    if(subtree.size() <= subsample_s){
        return;
    }else{
        splitPoint = getSplitPoint(subtree);
        filter(subtree, subLeft, splitPoint);
        filter(subtree, subRight, splitPoint);
        return;
    }
};

/* Divide os nodos a direita e a esquerda, de acordo
   o splitPoint
   Se splitpoint for positivo, direciona a direita
                 for negativo, direciona a esquerda */
void filter(MtxAdj &subtree, MtxAdj &direction, const float splitPoint){

};

float getSplitPoint(MtxAdj &subtree){
    std::srand(std::time(nullptr));
    int random_value = std::rand();

    return random_value;
}

int Article::path_length(
    float x, 
    MtxAdj & subtree, 
    const int height_limit,
    int path_length){
        return path_length;
}

/* Obtem arvores binarias, escolhendo vertices
   de maneira aleatoria, a partir de um grafo */
MtxAdj sample(MtxAdj graph, const int size){
    ArestaAdj a = graph[0];
    MtxAdj::iterator iterator;
    MtxAdj grafo;

    grafo.insert(iterator, a);
    return grafo;
};