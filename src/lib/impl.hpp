#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>

#include "graph.hpp"
#include "article.hpp"

#define COLUMNS_SIZE 28

namespace Tag {
    void construirGrafo(const char *, std::vector<std::vector<Aresta>> &);
    void obterComunidades(std::vector<std::vector<Aresta>> &);
    void extrairCaracterizacao();
    void identificarComunidadeAnomala(std::vector<std::vector<Aresta>> &);
    void detectarAnomalia();
};