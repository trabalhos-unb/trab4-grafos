#include "impl.hpp"

void Tag::construirGrafo(const char *nomeArquivo, MtxAdj &mtx){
  std::ifstream arquivo;
  std::string linhaLeitura;
  arquivo.open(nomeArquivo);
  std::vector<Aresta> linhaMtx;
  //colunas do csv
  int time;
  double vColuna;
  float amount;

  if (!arquivo.is_open())
  {
      std::cout << "Nao foi possivel abrir o arquivo" << '\n';
      exit(EXIT_FAILURE);
  }else{
    //Le arquivo CSV para a matrix
    std::vector<Aresta>::iterator iterator;

    while(!arquivo.eof()){
      //Percorre colunas da matrix que representa grafo
      for(int index = 0 ; index < COLUMNS_SIZE; index++){
          linhaMtx = mtx[index];
          //Le coluna Time
          getline(arquivo, linhaLeitura, ',');
          time = std::stoi(linhaLeitura);
          //Le v1-v28 coluna
          iterator = linhaMtx.begin();
          for(int i = 1; i <= 28; i++){
            getline(arquivo, linhaLeitura, ',');
            vColuna = std::stod(linhaLeitura);
            iterator = linhaMtx.insert (iterator, *(new Aresta(vColuna)) );
          }
          //Le amount
          getline(arquivo, linhaLeitura);
          amount = std::stof(linhaLeitura);
        }
      }
  }
}

void Tag::identificarComunidadeAnomala(MtxAdj &grafo){
  Forest f = Article::iforest(grafo, DEFAULT_NUMBER_TREES, DEFAULT_SUBSAMPLE_SIZE);
  for(int i = 0; i < f.size() ; i++){
    //Article::path_length(f[i]);
  }
}