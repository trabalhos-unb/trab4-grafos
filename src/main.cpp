/*****************************************************************************\
|* Teoria e Aplicação de Grafos - A                                          *|
|* Dupla: Danilo Santos - 14/0135910, Bruno Helder - 15/0120338              *|
|*****************************************************************************|*/

#include <iostream>
#include "lib/impl.hpp"

int main(void)
{   
    std::vector<std::vector<Aresta>> mtx(COLUMNS_SIZE);
    Tag::construirGrafo("spec/data-mini.csv", mtx);
    return 0;
}
